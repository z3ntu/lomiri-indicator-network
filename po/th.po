# Thai translation for lomiri-indicator-network
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the lomiri-indicator-network package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-indicator-network\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-04-29 12:38+0300\n"
"PO-Revision-Date: 2020-06-29 14:52+0000\n"
"Last-Translator: PPNplus <ppnplus@protonmail.com>\n"
"Language-Team: Thai <https://translate.ubports.com/projects/ubports/"
"lomiri-indicator-network/th/>\n"
"Language: th\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2016-10-21 07:05+0000\n"

#: ../src/agent/SecretRequest.cpp:61
msgid "Connect to “%1”"
msgstr "เชื่อมต่อ \"%1\""

#: ../src/agent/SecretRequest.cpp:66
msgid "WPA"
msgstr ""

#: ../src/agent/SecretRequest.cpp:68
msgid "WEP"
msgstr "WEP"

#: ../src/agent/SecretRequest.cpp:74
msgid "Connect"
msgstr "เชื่อมต่อ"

#: ../src/agent/SecretRequest.cpp:75
msgid "Cancel"
msgstr "ยกเลิก"

#: ../src/indicator/menuitems/wifi-link-item.cpp:109
msgid "Other network…"
msgstr "เครือข่ายอื่น ๆ…"

#: ../src/indicator/menuitems/wwan-link-item.cpp:90
msgid "No SIM"
msgstr "ไม่มี SIM"

#: ../src/indicator/menuitems/wwan-link-item.cpp:97
msgid "SIM Error"
msgstr "SIM ผิดพลาด"

#: ../src/indicator/menuitems/wwan-link-item.cpp:105
msgid "SIM Locked"
msgstr "SIM ถูกล็อก"

#: ../src/indicator/menuitems/wwan-link-item.cpp:118
msgid "Unregistered"
msgstr "ไม่ได้ลงทะเบียน"

#: ../src/indicator/menuitems/wwan-link-item.cpp:123
msgid "Unknown"
msgstr "ไม่รู้จัก"

#: ../src/indicator/menuitems/wwan-link-item.cpp:128
msgid "Denied"
msgstr "ถูกปฏิเสธ"

#: ../src/indicator/menuitems/wwan-link-item.cpp:133
msgid "Searching"
msgstr "กำลังค้นหา"

#: ../src/indicator/menuitems/wwan-link-item.cpp:145
msgid "No Signal"
msgstr "ไม่มีสัญญาณ"

#: ../src/indicator/menuitems/wwan-link-item.cpp:157
#: ../src/indicator/menuitems/wwan-link-item.cpp:164
msgid "Offline"
msgstr "ออฟไลน์"

#. TODO Move this into a new class
#: ../src/indicator/factory.cpp:187
msgid "Wi-Fi"
msgstr "Wi-Fi"

#. TODO Move this into a new class
#: ../src/indicator/factory.cpp:198
msgid "Flight Mode"
msgstr "โหมดเครื่องบิน"

#: ../src/indicator/factory.cpp:208
msgid "Cellular data"
msgstr "ข้อมูลเซลลูลาร์"

#. TODO Move this into a new class
#: ../src/indicator/factory.cpp:219
msgid "Hotspot"
msgstr "ฮอตสปอต"

#: ../src/indicator/sim-unlock-dialog.cpp:144
msgid "Sorry, incorrect %{1} PIN."
msgstr ""

#: ../src/indicator/sim-unlock-dialog.cpp:149
#: ../src/indicator/sim-unlock-dialog.cpp:178
msgid "This will be your last attempt."
msgstr ""

#: ../src/indicator/sim-unlock-dialog.cpp:151
msgid ""
"If %{1} PIN is entered incorrectly you will require your PUK code to unlock."
msgstr "หากใส่ PIN %{1} ไม่ถูกต้อง คุณจะต้องใช้รหัส PUK เพื่อปลดล็อก"

#: ../src/indicator/sim-unlock-dialog.cpp:161
msgid "Sorry, your %{1} is now blocked."
msgstr "ขออภัย %{1} ของคุณถูกบล็อก"

#: ../src/indicator/sim-unlock-dialog.cpp:166
msgid "Please enter your PUK code to unblock SIM card."
msgstr "กรุณาใส่รหัส PUK ของคุณเพื่อปลดบล็อก SIM การ์ด"

#: ../src/indicator/sim-unlock-dialog.cpp:168
msgid "You may need to contact your network provider for PUK code."
msgstr "คุณอาจต้องติดต่อผู้ให้บริการเครือข่ายของคุณเพื่อขอรับรหัส PUK"

#: ../src/indicator/sim-unlock-dialog.cpp:176
#: ../src/indicator/sim-unlock-dialog.cpp:190
msgid "Sorry, incorrect PUK."
msgstr "ขออภัย PUK ไม่ถูกต้อง"

#: ../src/indicator/sim-unlock-dialog.cpp:180
msgid ""
"If PUK code is entered incorrectly, your SIM card will be blocked and needs "
"replacement."
msgstr ""
"หากใส่รหัส PUK ไม่ถูกต้อง SIM การ์ดของคุณจะถูกบล็อกและจะต้องทำการเปลี่ยนใหม่"

#: ../src/indicator/sim-unlock-dialog.cpp:182
msgid "Please contact your network provider."
msgstr "กรุณาติดต่อผู้ให้บริการเครือข่ายของคุณ"

#: ../src/indicator/sim-unlock-dialog.cpp:192
msgid "Your SIM card is now permanently blocked and needs replacement."
msgstr "SIM การ์ดของคุณถูกบล็อกถาวรและต้องเปลี่ยนใหม่"

#: ../src/indicator/sim-unlock-dialog.cpp:194
msgid "Please contact your service provider."
msgstr "กรุณาติดต่อผู้ให้บริการของคุณ"

#: ../src/indicator/sim-unlock-dialog.cpp:218
msgid "Sorry, incorrect PIN"
msgstr "ขออภัย PIN ไม่ถูกต้อง"

#: ../src/indicator/sim-unlock-dialog.cpp:230
msgid "Sorry, incorrect PUK"
msgstr "ขออภัย PUK ไม่ถูกต้อง"

#: ../src/indicator/sim-unlock-dialog.cpp:292
msgid "Enter %{1} PIN"
msgstr "ใส่ PIN %{1}"

#: ../src/indicator/sim-unlock-dialog.cpp:300
msgid "Enter PUK code"
msgstr "ใส่รหัส PUK"

#: ../src/indicator/sim-unlock-dialog.cpp:304
msgid "Enter PUK code for %{1}"
msgstr "ใส่รหัส PUK สำหรับ %{1}"

#: ../src/indicator/sim-unlock-dialog.cpp:315
#, c-format
msgid "1 attempt remaining"
msgid_plural "%d attempts remaining"
msgstr[0] ""
msgstr[1] ""

#: ../src/indicator/sim-unlock-dialog.cpp:326
msgid "Enter new %{1} PIN"
msgstr ""

#: ../src/indicator/sim-unlock-dialog.cpp:334
msgid "Confirm new %{1} PIN"
msgstr ""

#: ../src/indicator/sim-unlock-dialog.cpp:373
msgid "PIN codes did not match."
msgstr ""

#: ../src/indicator/vpn-status-notifier.cpp:48
msgid "The VPN connection '%1' failed."
msgstr ""

#: ../src/indicator/vpn-status-notifier.cpp:50
msgid ""
"The VPN connection '%1' failed because the network connection was "
"interrupted."
msgstr ""

#: ../src/indicator/vpn-status-notifier.cpp:51
msgid ""
"The VPN connection '%1' failed because the VPN service stopped unexpectedly."
msgstr ""

#: ../src/indicator/vpn-status-notifier.cpp:52
msgid ""
"The VPN connection '%1' failed because the VPN service returned invalid "
"configuration."
msgstr ""

#: ../src/indicator/vpn-status-notifier.cpp:53
msgid ""
"The VPN connection '%1' failed because the connection attempt timed out."
msgstr ""

#: ../src/indicator/vpn-status-notifier.cpp:54
msgid ""
"The VPN connection '%1' failed because the VPN service did not start in time."
msgstr ""

#: ../src/indicator/vpn-status-notifier.cpp:55
msgid "The VPN connection '%1' failed because the VPN service failed to start."
msgstr ""

#: ../src/indicator/vpn-status-notifier.cpp:56
msgid "The VPN connection '%1' failed because there were no valid VPN secrets."
msgstr ""

#: ../src/indicator/vpn-status-notifier.cpp:57
msgid "The VPN connection '%1' failed because of invalid VPN secrets."
msgstr ""

#: ../src/indicator/vpn-status-notifier.cpp:68
msgid "VPN Connection Failed"
msgstr ""

#: ../src/indicator/sections/wwan-section.cpp:99
msgid "Cellular settings…"
msgstr "การตั้งค่าเซลลูลาร์…"

#: ../src/indicator/sections/vpn-section.cpp:140
msgid "VPN settings…"
msgstr "การตั้งค่า VPN…"

#: ../src/indicator/sections/wifi-section.cpp:65
msgid "Wi-Fi settings…"
msgstr "การตั้งค่า Wi-Fi…"

#. TRANSLATORS: this is the indicator title shown on the top header of the indicator area
#: ../src/indicator/root-state.cpp:306
msgid "Network"
msgstr "เครือข่าย"

#: ../src/indicator/nmofono/vpn/vpn-manager.cpp:81
msgid "VPN connection %1"
msgstr ""
